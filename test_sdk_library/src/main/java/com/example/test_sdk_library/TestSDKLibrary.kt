package com.example.test_sdk_library

class TestSDKLibrary {

    fun addTwoNumbers (x: Double, y: Double): Double {
        return (x + y)
    }

    fun subtractTwoNumbers (x: Double, y: Double): Double {
        return (x - y)
    }

}